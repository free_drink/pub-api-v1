import json
import random
import string
import requests

# Number of random pubs to generate
NUM_PUBS = 3

# "random" data lists
pubs_list = ["Pub", "Bar", "Tavern", "Alehouse", "Saloon", "Inn", "Lounge", "Brewery", "Cantina", "Canteen", "Club", "Dive", "Gastropub", "Roadhouse", "Speakeasy", "Watering Hole", "Beer Garden", "Brewpub", "Drinkery", "Nightclub", "Public House", "Saloon", "Taproom", "Taphouse", "Beerhouse", "Brew House", "Cocktail Lounge", "Drink House", "Gin Mill"]
pubs_cont = ["House", "Inn", "Pub", "Bar", "Tavern", "Alehouse", "Saloon", "Lounge", "Brewery", "Cantina", "Canteen", "Club", "Dive", "Gastropub", "Roadhouse", "Speakeasy", "Watering Hole", "Beer Garden", "Brewpub", "Drinkery", "Nightclub", "Public House", "Saloon", "Taproom", "Taphouse", "Beerhouse", "Brew House", "Cocktail Lounge", "Drink House", "Gin Mill"]
adjective_list = ["Cozy", "Friendly", "Charming", "Welcoming", "Warm", "Inviting", "Homely", "Comfortable", "Pleasant", "Attractive", "Appealing", "Delightful", "Agreeable", "Enjoyable", "Enticing", "Alluring", "Engaging", "Captivating", "Enchanting", "Fascinating", "Interesting", "Appealing", "Pleasing", "Satisfying", "Gratifying", "Rewarding", "Fulfilling", "Heartwarming", "Heartening", "Inspiring", "Uplifting", "Encouraging", "Reassuring", "Cheering", "Comforting", "Soothing", "Solacing"] 
name_list = ["John", "Jane", "Jack", "Jill", "Jim", "Jenny", "Joe", "Judy", "Jerry", "Jill", "Jeff", "Jen", "Jesse", "Jasmine", "Jasper", "Jade", "Jax"]
seconds_name_list = ["Doe", "Smith", "Johnson", "Brown", "Williams", "Jones", "Garcia", "Miller", "Davis", "Rodriguez", "Martinez", "Hernandez", "Lopez", "Gonzalez", "Wilson", "Anderson", "Thomas", "Taylor", "Moore", "Jackson", "Martin", "Lee", "Perez", "Thompson", "White", "Harris", "Sanchez", "Clark", "Ramirez", "Lewis", "Robinson", "Walker", "Young", "Allen", "King", "Wright", "Scott", "Torres", "Nguyen", "Hill", "Flores", "Green", "Adams", "Nelson", "Baker", "Hall", "Rivera", "Campbell", "Mitchell", "Carter", "Roberts", "Gomez", "Phillips", "Evans", "Turner", "Diaz", "Parker", "Cruz", "Edwards", "Collins", "Reyes", "Stewart", "Morris", "Morales", "Murphy", "Cook", "Rogers", "Gutierrez", "Ortiz", "Morgan", "Cooper", "Peterson", "Bailey", "Reed", "Kelly", "Howard", "Ramos", "Kim", "Cox", "Ward", "Richardson", "Watson", "Brooks", "Chavez", "Wood", "James", "Bennett", "Gray", "Mendoza", "Ruiz", "Hughes", "Price", "Alvarez", "Castillo", "Sanders", "Patel", "Myers", "Long", "Ross", "Foster", "Jimenez", "Powell", "Jenkins", "Perry", "Russell", "Sullivan", "Bell", "Cole", "Bennett", "Singh", "Silva", "Morgan", "Harrison", "Hernandez", "Gutierrez", "Fernandez", "Ramos", "Reyes", "Gonzales", "Gonzalez", "Gomez", "Garcia", "Rodriguez", "Martinez", "Hernandez", "Lopez", "Gonzalez", "Wilson"]

# List to store generated pub data
pubs = []

# Generate random pub data
for _ in range(NUM_PUBS):
    pub = {
        "name": f"The {random.choice(adjective_list)} {random.choice(pubs_list)}",
        "owner": f"{random.choice(name_list)} {random.choice(seconds_name_list)}",
        "image_url": f"https://placeimg.com/640/480/pub",
        "description": f"A {random.choice(adjective_list)} {random.choice(pubs_list)} with a {random.choice(adjective_list)} atmosphere.",
        "schedule": "Monday-Friday: 5pm-11pm, Saturday-Sunday: 12pm-12am",
        "serves_beer": random.choice([True, False]),
        "available_discounts": random.choice([True, False]),
        "latitude": f"{random.uniform(30.0, 50.0):.6f}",
        "longitude": f"{random.uniform(-120.0, -60.0):.6f}",
    }
    pubs.append(pub)

for pub in pubs:
  response = requests.post("http://localhost:8080/pubs", json=pub)
  print(response.json())
