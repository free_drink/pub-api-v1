package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

// Pub struct represents a pub entity
type Pub struct {
	gorm.Model
	Name               string `json:"name,omitempty"`
	Owner              string `json:"owner,omitempty"`
	ImageURL           string `json:"image_url,omitempty"`
	Description        string `json:"description,omitempty"`
	Schedule           string `json:"schedule,omitempty"`
	ServesBeer         bool   `json:"serves_beer,omitempty"`
	AvailableDiscounts bool   `json:"available_discounts,omitempty"`
	Latitude           string `json:"latitude,omitempty"`
	Longitude          string `json:"longitude,omitempty"`
}

func init() {
	var err error
	db, err = gorm.Open(postgres.Open(os.Getenv("URI")), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected to the database")
	db.AutoMigrate(&Pub{})
	log.Println("Tables initialized")
}

func main() {
	router := mux.NewRouter()

	// Endpoints
	router.HandleFunc("/pubs", getPubs).Methods("GET")
	router.HandleFunc("/pubs/{id}", getPub).Methods("GET")
	router.HandleFunc("/pubs", createPub).Methods("POST")
	router.HandleFunc("/pubs/{id}", updatePub).Methods("PUT")
	router.HandleFunc("/pubs/{id}", deletePub).Methods("DELETE")

	log.Println("Starting server on :8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func getPubs(w http.ResponseWriter, r *http.Request) {
	log.Println("GET /pubs")
	var pubs []Pub
	db.Find(&pubs)
	json.NewEncoder(w).Encode(pubs)
}

func getPub(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	log.Printf("GET /pubs/%s", id)

	var pub Pub
	db.First(&pub, id)
	if pub.ID == 0 {
		http.Error(w, "Pub not found", http.StatusNotFound)
		return
	}
	json.NewEncoder(w).Encode(pub)
}

func createPub(w http.ResponseWriter, r *http.Request) {
	var pub Pub
	_ = json.NewDecoder(r.Body).Decode(&pub)

	log.Printf("Received request to create pub: Name - %s, Owner - %s", pub.Name, pub.Owner)
	result := db.Where(&Pub{Name: pub.Name, Owner: pub.Owner}).First(&Pub{})
	// if there is no match the function fails (record not found)
	if result.Error != nil && result.Error != gorm.ErrRecordNotFound {
		log.Println(result.Error)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	if result.RowsAffected > 0 {
		log.Printf("Validation failed: Pub with the same name (%s) and owner (%s) already exists", pub.Name, pub.Owner)
		http.Error(w, "Pub with the same name and owner already exists", http.StatusBadRequest)
		return
	}

	err := db.Create(&pub).Error
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	log.Printf("Pub created successfully: Name - %s, Owner - %s", pub.Name, pub.Owner)

	json.NewEncoder(w).Encode(pub)
}

func updatePub(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	log.Printf("PUT /pubs/%s", id)

	var pub Pub
	err := json.NewDecoder(r.Body).Decode(&pub)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	pub.ID = mustParseUint(id)
	err = db.Save(&pub).Error
	if err != nil {
		log.Println(err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	json.NewEncoder(w).Encode(pub)
}

func deletePub(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	log.Printf("DELETE /pubs/%s", id)

	result := db.Delete(&Pub{}, id)
	if result.Error != nil {
		log.Println(result.Error)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent) // Successfully deleted
}

func mustParseUint(id string) uint {
	parsedID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		panic(err) // Handle error more gracefully in production
	}
	return uint(parsedID)
}
